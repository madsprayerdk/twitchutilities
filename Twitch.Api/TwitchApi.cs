﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Api.Core;
using Api.Twitch.Model;
using Api.Twitch.Model.FollowElements;
using Newtonsoft.Json;

namespace Api.Twitch
{
    public class TwitchApi
    {
        private readonly string _channel;
        private readonly string _oAuthKey;
        private readonly string _apiBaseUri = "https://api.twitch.tv/kraken/";

        public TwitchApi(string channel, string oAuthKey)
        {
            _channel = channel;
            _oAuthKey = oAuthKey;
        }

        public async Task<Channel> GetChannel()
        {
            return JsonConvert.DeserializeObject<Channel>(await HttpRequester.Get(_apiBaseUri, "channels/" + _channel));
        }

        public async Task<StreamInfo> GetStream()
        {
            return JsonConvert.DeserializeObject<StreamInfo>(await HttpRequester.Get(_apiBaseUri, "streams/" + _channel));
        }

        public async Task<SearchGames> SearchGames(string query)
        {
            return JsonConvert.DeserializeObject<SearchGames>(await HttpRequester.Get(_apiBaseUri, "search/games?q=" + query + "&type=suggest"));
        }

        public async Task<IList<Follower>> GetFollowers(int afterId = 0)
        {
            var followers = new List<Follower>();
            var follow = JsonConvert.DeserializeObject<Follow>(await HttpRequester.Get(_apiBaseUri, "channels/" + _channel + "/follows"));
            followers.AddRange(follow.Follows);

            var cursor = follow._cursor;
            while (followers.All(x => x.User._id != afterId))
            {
                var nextFollow = JsonConvert.DeserializeObject<Follow>(await HttpRequester.Get(_apiBaseUri, "channels/" + _channel + "/follows?cursor=" + cursor + "&direction=DESC&limit=25"));
                cursor = nextFollow._cursor;
                followers.AddRange(nextFollow.Follows);

                if (follow._total == followers.Count)
                    break;
            }

            followers = followers.OrderBy(x => x.Created_At).ToList();

            if (afterId == 0)
                return followers;

            var index = followers.FindIndex(x => x.User._id == afterId);
            followers.RemoveRange(0, index + 1);
            return followers;
        }

        public async Task<bool> UpdateStatus(string status)
        {
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("channel[status]", status), 
            });

            return await HttpRequester.Put(_apiBaseUri, "channels/" + _channel + "?oauth_token=" + _oAuthKey, content);
        }

        public async Task<bool> UpdateGame(string game)
        {
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("channel[game]", game), 
            });

            return await HttpRequester.Put(_apiBaseUri, "channels/" + _channel + "?oauth_token=" + _oAuthKey, content);
        }
    }
}
