﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Api.Twitch.Model.Links;

namespace Api.Twitch.Model
{
    public class User
    {
        public UserLink _links { set; get; }
        public string Type { set; get; }
        public string Bio { set; get; }
        public string Logo { set; get; }
        public string Display_Name { set; get; }
        public DateTime Created_At { set; get; }
        public DateTime Updated_At { set; get; }
        public int _id { set; get; }
        public string Name { set; get; }
    }
}
