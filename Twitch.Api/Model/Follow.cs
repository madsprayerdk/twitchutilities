﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.Twitch.Model.FollowElements;
using Api.Twitch.Model.Links;

namespace Api.Twitch.Model
{
    public class Follow
    {
        public int _total { set; get; }
        public FollowLinks _links { set; get; }
        public string _cursor { set; get; }
        public IList<Follower> Follows { set; get; }
    }
}
