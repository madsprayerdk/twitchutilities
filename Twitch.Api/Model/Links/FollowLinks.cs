﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Twitch.Model.Links
{
    public class FollowLinks
    {
        public string Self { set; get; }
        public string Next { set; get; }
    }
}
