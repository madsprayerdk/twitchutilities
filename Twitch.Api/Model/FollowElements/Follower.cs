﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Twitch.Model.FollowElements
{
    public class Follower
    {
        public DateTime Created_At { set; get; }
        public FollowerLinks _links { set; get; }
        public bool notifications { set; get; }
        public User User { set; get; }
    }
}
