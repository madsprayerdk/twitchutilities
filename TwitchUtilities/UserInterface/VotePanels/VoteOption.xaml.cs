﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TwitchUtilities.ViewModel;

namespace TwitchUtilities.UserInterface.VotePanels
{
    /// <summary>
    /// Interaction logic for VoteOption.xaml
    /// </summary>
    public partial class VoteOption : UserControl
    {
        public delegate void OptionsRemovedClicked(object sender, EventArgs args);
        public event OptionsRemovedClicked RemoveClicked;

        public delegate void OptionChangeKeyUp(object sender, EventArgs args);
        public event OptionChangeKeyUp Updated;

        public VoteOptionViewModel _model;

        public VoteOption()
        {
            InitializeComponent();
            _model = new VoteOptionViewModel();
            DataContext = _model;
        }

        public new string Name
        {
            set { _model.Name = value; }
            get { return _model.Name;  }
        }

        public string Value
        {
            set { _model.Value = value; }
            get { return _model.Value;  }
        }

        public int Percent
        {
            set { _model.Percent = value; }
            get { return _model.Percent; }
        }

        public int NumberOfVotes
        {
            set { _model.NumberOfVotes = value; }
            get { return _model.NumberOfVotes; }
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            RemoveClicked?.Invoke(this, null);
        }

        private void Option_OnKeyUp(object sender, KeyEventArgs e)
        {
            Updated?.Invoke(this, null);
        }
    }
}
