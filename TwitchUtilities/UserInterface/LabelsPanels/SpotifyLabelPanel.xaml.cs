﻿using System;
using System.Diagnostics;
using System.IO;
using System.Timers;
using System.Windows;
using Api.Spotify;

namespace TwitchUtilities.UserInterface.LabelsPanels
{
    /// <summary>
    /// Interaction logic for SpotifyLabelPanel.xaml
    /// </summary>
    public partial class SpotifyLabelPanel
    {
        private readonly SpotifyApi _spotify;
        private Timer _udateTimer;

        private string _oldOutput = "";

        public SpotifyLabelPanel()
        {
            InitializeComponent();
            _spotify = new SpotifyApi("tu.spotilocal.com");
        }

        private async void SpotifyLabelPanel_OnLoaded(object sender, RoutedEventArgs e)
        {
            Process[] pname = Process.GetProcessesByName("SpotifyWebHelper");
            if (pname.Length == 0)
            {
                DisplayException();

                Dispatcher.Invoke(() =>
                {
                    Data.Visibility = Visibility.Collapsed;
                    Retry.Visibility = Visibility.Visible;
                });
                

                return;
            }

            Dispatcher.Invoke(() =>
            {
                Data.Visibility = Visibility.Visible;
                Retry.Visibility = Visibility.Collapsed;
            });

            await _spotify.Init();

            _udateTimer = new Timer
            {
                Interval = 1000,
                AutoReset = true
            };

            _udateTimer.Elapsed += _updateTimer_Elapsed;
            _udateTimer.Start();
        }

        private async void _updateTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Api.Spotify.Responses.Status status;
            try
            {
                status = await _spotify.GetStatus();
            }
            catch (Exception)
            {
                _udateTimer.Stop();

                DisplayException();
                SpotifyLabelPanel_OnLoaded(this, null);
                return;
            }

            if (status.Track != null)
            {
                var songLength = TimeSpan.FromSeconds(status.Track.Length);
                var playedSpan = TimeSpan.FromSeconds(status.Playing_Position);

                Dispatcher.Invoke(() =>
                {
                    if (status.Playing)
                        SpotifyTimer.Content = "(" + playedSpan.Minutes + ":" + playedSpan.Seconds.ToString("D2") + " / " + songLength.Minutes + ":" + songLength.Seconds.ToString("D2") + ")";
                    else
                        SpotifyTimer.Content = "(-:-- / -:--)";
                });
            }

            var output = status.Playing ? status.Track.Track_Resource.Name + " - " + status.Track.Artist_Resource.Name : "No song playing.";

            if (output == _oldOutput)
                return;

            _oldOutput = output;

            Dispatcher.Invoke(() =>
            {
                SpotifyOutput.Content = output;
            });
            try
            {
                File.WriteAllText("Labels/SpotifyInfo.txt", output);
            }
            catch (Exception)
            {
                DisplayException();
            }
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            SpotifyLabelPanel_OnLoaded(this, null);
        }

        private void DisplayException()
        {
            Dispatcher.Invoke(() =>
            {
                var parent = (MainWindow)Window.GetWindow(this);

                if (parent == null)
                    return;

                var now = DateTime.Now;
                parent.ExceptionText.Text = "[" + now.ToShortTimeString() + "] " + "Can't connect to Spotify (SpotifyWebHelper not running).";
            });
        }
    }
}
