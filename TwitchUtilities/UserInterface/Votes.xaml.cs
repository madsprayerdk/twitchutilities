﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using ChatSharp;
using ChatSharp.Events;
using TwitchUtilities.Properties;
using TwitchUtilities.UserInterface.VotePanels;
using TwitchUtilities.ViewModel;

namespace TwitchUtilities.UserInterface
{
    /// <summary>
    /// Interaction logic for Votes.xaml
    /// </summary>
    public partial class Votes
    {
        private readonly IrcClient _irc;
        private readonly Dictionary<string, VoteOption> _voteResults = new Dictionary<string, VoteOption>();
        private bool _activeVote;
        private readonly Timer _getUserListTimer;

        private readonly ObservableCollection<VoteOption> _options;
        private readonly VotesViewModel _viewModel;

        public Votes()
        {
            InitializeComponent();

            _viewModel = new VotesViewModel
            {
                StartPauseEnabled = false,
                StartPauseLabel = "Start Vote",
                AddOptionEnabled = true,
                ResetVoteEnabled = true
            };
            DataContext = _viewModel;

            _options = new ObservableCollection<VoteOption>();
            _activeVote = false;

            ContentControl.DataContext = _options;

            _getUserListTimer = new Timer
            {
                AutoReset = true,
                Interval = 20000
            };

            _getUserListTimer.Elapsed += getUserListTimer_Elapsed;

            _irc = new IrcClient("irc.twitch.tv", new IrcUser(Settings.Default.CurrentChannel, Settings.Default.CurrentChannel, "oauth:" + Settings.Default.OAuthToken));
            _irc.ChannelMessageRecieved += Irc_ChannelMessageRecieved;
            _irc.ConnectionComplete += _irc_ConnectionComplete;
            _irc.ConnectAsync();

            UpdateVote();
        }

        private void getUserListTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var channel = _irc.Channels.FirstOrDefault(x => x.Name == "#" + Settings.Default.CurrentChannel);

            if (channel == null)
                return;

            var users = channel.Users.Select(x => x.Nick).ToList();

            foreach (var voteResult in _voteResults.ToList())
            {
                if (!users.Contains(voteResult.Key))
                    _voteResults.Remove(voteResult.Key);
            }

            UpdateVote();
        }

        void _irc_ConnectionComplete(object sender, EventArgs e)
        {
            _irc.UserJoinedChannel += IrcOnUserJoinedChannel;
            _irc.JoinChannel("#" + Settings.Default.CurrentChannel);
        }

        private void IrcOnUserJoinedChannel(object sender, ChannelUserEventArgs channelUserEventArgs)
        {
            _viewModel.StartPauseEnabled = true;
        }

        void Irc_ChannelMessageRecieved(object sender, PrivateMessageEventArgs e)
        {
            if (!_activeVote)
                return;

            var message = e.PrivateMessage.Message.ToLower();
            var votedOption = _options.FirstOrDefault(x => message.Contains(x.Value.ToLower()));

            if (votedOption == null)
                return;

            if (_voteResults.ContainsKey(e.PrivateMessage.User.Nick))
                _voteResults[e.PrivateMessage.User.Nick] = votedOption;
            else
                _voteResults.Add(e.PrivateMessage.User.Nick, votedOption);

            UpdateVote();
        }

        private void AddOption_Click(object sender, RoutedEventArgs e)
        {
            var option = new VoteOption();
            option.RemoveClicked += Option_RemoveClicked;
            option.Updated += Option_Updated;

            _options.Add(option);
            UpdateVote();
        }

        private void Option_Updated(object sender, EventArgs args)
        {
            UpdateVote();
        }

        void Option_RemoveClicked(object sender, EventArgs args)
        {
            var option = sender as VoteOption;

            if (option == null)
                return;

            _options.Remove(option);
            _voteResults.Clear();

            UpdateVote();
        }

        private void StartPause_Click(object sender, RoutedEventArgs e)
        {
            if (_activeVote)
            {
                _viewModel.StartPauseLabel = "Start Vote";
                _activeVote = false;
                _viewModel.AddOptionEnabled = true;
                _viewModel.ResetVoteEnabled = true;
                _getUserListTimer.Stop();

                foreach (var option in _options)
                {
                    option.Remove.IsEnabled = true;
                }
            }
            else
            {
                _viewModel.StartPauseLabel = "Pause Vote";
                _activeVote = true;
                _viewModel.AddOptionEnabled = false;
                _viewModel.ResetVoteEnabled = false;
                _getUserListTimer.Start();

                if(_viewModel.ShowOnStream)
                    UpdateVote();

                foreach (var option in _options)
                {
                    option.Remove.IsEnabled = false;
                }
            }
        }

        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.VoteName = "";
            _options.Clear();
            _voteResults.Clear();
            UpdateVote();
        }

        private void UpdateVote()
        {
            var total = _voteResults.Count;

            foreach (var voteOption in _options)
            {
                var currentCount = _voteResults.Count(x => Equals(x.Value, voteOption));
                voteOption.NumberOfVotes = currentCount;
                var percent = total == 0 ? 0 : (currentCount * 100) / total;
                voteOption.Percent = percent;
            }

            if (!File.Exists("Labels/VoteResult.txt"))
                File.Create("Labels/VoteResult.txt");

            if (_viewModel.ShowOnStream)
            {
                var outputString = _viewModel.VoteName + "\n";

                foreach (VoteOption option in _options)
                {
                    outputString += option.Name + ": " + option.Value + " - " + option.Percent + "%\n";
                }

                File.WriteAllText("Labels/VoteResult.txt", outputString);
            }
            else
                File.WriteAllText("Labels/VoteResult.txt", "");
        }

        private void ShowOnStream_OnChecked(object sender, RoutedEventArgs e)
        {
            UpdateVote();
        }

        private void VoteName_OnKeyUp(object sender, KeyEventArgs e)
        {
            UpdateVote();
        }
    }
}
