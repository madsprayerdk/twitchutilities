﻿namespace TwitchUtilities.ViewModel
{
    public class VoteOptionViewModel
    {
        public string Name { set; get; }
        public string Value { set; get; }
        public int Percent { set; get; }
        public int NumberOfVotes { set; get; }
    }
}
