﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TwitchUtilities.Annotations;

namespace TwitchUtilities.ViewModel
{
    public class OAuthLoginViewModel : INotifyPropertyChanged
    {
        private string _statusText;
        public string StatusText
        {
            set
            {
                if (_statusText != value)
                {
                    _statusText = value;
                    OnPropertyChanged();
                }
            }
            get
            {
                return _statusText;
            }
        }

        private bool _loginEnabled;
        public bool LoginEnabled {
            set
            {
                if (_loginEnabled != value)
                {
                    _loginEnabled = value;
                    OnPropertyChanged();
                }
            }
            get
            {
                return _loginEnabled;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
