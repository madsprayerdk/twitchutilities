﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TwitchUtilities.Annotations;

namespace TwitchUtilities.ViewModel
{
    public class VotesViewModel : INotifyPropertyChanged
    {
        public string VoteName { set; get; }
        public bool ShowOnStream { set; get; }

        private bool _startPauseEnabled;
        public bool StartPauseEnabled
        {
            set
            {
                if (_startPauseEnabled != value)
                {
                    _startPauseEnabled = value;
                    OnPropertyChanged();
                }
            }
            get { return _startPauseEnabled; }
        }

        private string _startPauseLabel;
        public string StartPauseLabel
        {
            set
            {
                if (_startPauseLabel != value)
                {
                    _startPauseLabel = value;
                    OnPropertyChanged();
                }
            }
            get { return _startPauseLabel; }
        }

        private bool _addOptionEnabled;
        public bool AddOptionEnabled
        {
            set
            {
                if (_addOptionEnabled != value)
                {
                    _addOptionEnabled = value;
                    OnPropertyChanged();
                }
            }
            get { return _addOptionEnabled; }
        }

        private bool _resetVoteEnabled;
        public bool ResetVoteEnabled
        {
            set
            {
                if (_resetVoteEnabled != value)
                {
                    _resetVoteEnabled = value;
                    OnPropertyChanged();
                }
            }
            get { return _resetVoteEnabled; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
