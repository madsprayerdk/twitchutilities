﻿using System;
using System.IO;
using System.Windows;

namespace TwitchUtilities
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            if (!Directory.Exists("Labels"))
                Directory.CreateDirectory("Labels");

            if (!File.Exists("Labels/Countdown.txt"))
                File.Create("Labels/Countdown.txt");

            if (!File.Exists("Labels/CustomLabel.txt"))
                File.Create("Labels/CustomLabel.txt");

            if (!File.Exists("Labels/SpotifyInfo.txt"))
                File.Create("Labels/SpotifyInfo.txt");

            if (!File.Exists("Labels/VoteResult.txt"))
                File.Create("Labels/VoteResult.txt");

            InitializeComponent();            
        }
    }
}